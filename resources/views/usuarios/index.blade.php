@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Correo Electronico</th>
                    <th scope="col">Administrador</th>

                  </tr>
                </thead>
                <tbody>
                    @foreach ( $usuarios as $usuario )
                    <tr>
                        <th scope="row">{{ $usuario->id}}</th>
                        <td>{{ $usuario->name }}</td>
                        <td>{{ $usuario->apellido }}</td>
                        <td>{{ $usuario->email }}</td>
                      <td>
                            <!-- Example single danger button -->
                        <div class="btn-group">
                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actiones
                                </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Editar</a>
                                <a class="dropdown-item" href="#">Ver</a>
                                <a class="dropdown-item" href="#">Eliminar</a>
                            </div>
                        </div>
                      </td>
                    </tr>      
                    @endforeach
                 
                 </tbody>
              </table>
        </div>

    </div>
</div>

@endsection