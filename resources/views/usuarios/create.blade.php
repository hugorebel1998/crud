@extends('layouts.app')
@section('content')
    
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                <h3 class="text-center">Crer usuario</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('usuarios.store') }}" method="POST"  autocomplete="off">
                        @csrf
                        <div class="row">
                         <div class="col-md-6">
                             <label for="nombre">Nombre</label>
                             <input 
                             type="text" 
                             name="nombre"
                             class="form-control @error('nombre') is-invalid @enderror"
                             placeholder="Ingresar nombre">
                            @error('nombre')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                         </div>

                         <div class="col-md-6">
                            <label for="nombre">Apellido</label>
                            <input 
                            type="text" 
                            name="apellido"
                            class="form-control @error('apellido') is-invalid @enderror"
                            placeholder="Ingresar apellido">
                            @error('apellido')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="col-md-6 mt-3">
                            <label for="nombre">Correo electrónico</label>
                            <input 
                            type="email" 
                            name="correo_electronico"
                            class="form-control @error('correo_electronico') is-invalid @enderror"
                            >
                            @error('correo_electronico')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <span class="form-text text-muted">admin@admin.com</span>
                        </div>

                        <div class="col-md-6 mt-3">
                            <label for="nombre">Contraseña</label>
                            <input 
                            type="password" 
                            name="contraseña"
                            class="form-control @error('contraseña') is-invalid @enderror"
                            >
                            @error('contraseña')
                              <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <span class="form-text text-muted">Minimo 8 caracteres</span>
                        </div>

                        <div class="col-md-6 mt-3">
                            <label for="nombre">Confirmar contraseña</label>
                            <input 
                            type="password" 
                            name="confirmar_contraseña"
                            class="form-control @error('confirmar_contraseña') is-invalid @enderror"
                            >
                            @error('confirmar_contraseña')
                              <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

        
                        </div>
                        <div class="text-center mt-4">
                            <button type="submit" class="btn btn-sm btn-primary">Guardar</button>

                        </div>
                    </form>

                </div>

            </div>
        </div>

    </div>

</div>






@endsection