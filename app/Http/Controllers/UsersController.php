<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;


class UsersController extends Controller
{

    public function index()
    {
        // $usuarios = User::where('id', '!=', 1)->orderBy('id', 'desc')->get();
        // return view('usuarios.index', compact('usuarios'));
        $usuarios = User::all();
        return $usuarios;
    }

    public function create()
    {
        return view('usuarios.create');
    }

    public function store(UserRequest $request)
    {

        // $usuario = new User();

        // $usuario->name     = $request->nombre;
        // $usuario->apellido = $request->apellido;
        // $usuario->email    = $request->correo_electronico;
        // $usuario->password = bcrypt($request->contraseña);
        $usuario = User::create($request->all());
        return $usuario;

        // if ($usuario->save()) {

        //     $usuario->name     = $request->nombre;
        //     $usuario->apellido = $request->apellido;
        //     $usuario->email    = $request->correo_electronico;
        //     $usuario->password = bcrypt($request->contraseña);

        //     if ($usuario->save()) {

        //         toastr()->success('Data has been saved successfully!');

        //         return redirect()->route('usuarios.index');
        //     } else {
        //         toastr()->error('An error has occurred please try again later.');

        //         return back();
        //     }
        // } else {
        //     toastr()->error('An error has occurred please try again later.');

        //     return redirect()->route('usuarios.index');
        // }

    }
}
