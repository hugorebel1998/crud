<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Ruta de usuarios 
// Route::get('/usuarios/index', 'UserController@index')->name('usuarios.index')->middleware('auth');
// Route::get('/usuarios/create', 'UserController@create')->name('usuarios.create')->middleware('auth');
// Route::post('/usuarios/store', 'UserController@store')->name('usuarios.store')->middleware('auth');